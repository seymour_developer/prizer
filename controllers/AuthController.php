<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/12/2018
 * Time: 14:27
 */

namespace app\controllers;

use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\web\Controller;
use app\models\LoginForm;


class AuthController extends Controller
{

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        $this->layout = 'login.php';
        return $this->render('/auth/login', [
            'model' => $model,
        ]);
    }

    public function actionSignup(){
        $model = new SignupForm();
        if(Yii::$app->request->isPost){
            $model->load(Yii::$app->request->post());
            if($model->signup()){
                return $this->redirect(['auth/login']);
            }
        }
        $this->layout = 'login.php';
        return $this->render('signup', ['model' => $model]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        //return $this->goHome();
        return $this->redirect(['auth/login']);
    }
}