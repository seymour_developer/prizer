<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m181212_101119_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string()->defaultValue(null),
            'password' => $this->string(),
            'bonus_account' => $this->string(255)->defaultValue(null),
            'customer_id' => $this->integer()->defaultValue(null),
            'fund_id' => $this->integer()->defaultValue(null),
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
